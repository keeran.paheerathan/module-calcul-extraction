# Transformation matrix retriever

This program retrieves the transformation matrix between the reference lidar and the other lidars in the Phenomobile.  
It uses a pointcloud acquired by the Phenomobile to retrieve it.  
You should use an acquisition with planar objects oriented in different ways to find the right transformation matrix.  


## Installation

Install the packages located in requirements.txt  
Install the CSF package from here: https://github.com/jianboqi/CSF


## Usage

Command line argument are the following:


| Name | Description |
| ------ | ------ |
| h5_path | The path to the HDF5 file. |
| output | The path to the folder where the transformation matrix will be outputted. |
| lidar | The lidar for which we will retrieve the transformation matrix. Choose between `lms_2` or `lms_3`|
| imu_correction | If this parameter is present, the corrections from the IMU will be applied. |
| reversed | If the side lidar (lidar 3) is reversed. |
| measuring_head_180 | If the measuring head was rotated by 180° during the acquisition. |

Example commands:

`python retrieve_transformationmatrix.py --h5_path path --output output --lidar lms_2 --reversed`  
`python retrieve_transformationmatrix.py --h5_path path --output output --lidar lms_3 --imu_correction --reversed --measuring_head_180`
