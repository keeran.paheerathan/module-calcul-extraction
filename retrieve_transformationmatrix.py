import argparse
import math
import os
import time

import numpy as np
from h5_info import Geo, H5Info, constants
from mayavi import mlab
from scipy import optimize as optim
from sklearn.metrics import mean_squared_error

import utils
from lidar_calc import LidarCalc
from preprocess import preprocess_pointcloud

iteration = 1


def get_lidars_data(
    h5_path: str, isLidar3reversed: bool, imu_correction: bool, measuring_head_180: bool
) -> list:
    """Get the reference lidar data and pointcloud as well as the other lidar and the H5 data.

    Extracts the pointclouds and all the other data using the lidar extraction module.

    Args:
        h5_path (str): Path to the HDF5 file
        isLidar3reversed (bool): If the side lidar (lidar 3) is reversed
    """

    # Load the lidar data from the HDF5 file
    sensor_names = ["Lidar"]
    h5_info = H5Info()
    h5_info.load_data(h5_path, sensor_names)

    lidars = {}
    for sensor in h5_info.sensors:
        if sensor.type == constants.TYPE_LIDAR:
            image = sensor.images[0]
            lidars[sensor.description] = image.scans

    # Get the reference lidar pointcloud
    ref_lidar_pointcloud = LidarCalc.compute_lidar_positions(
        lidars["lms_1"],
        np.identity(4),
        lidars["lms_1"],
        Geo.to_dict_array(h5_info.geo),
        imu_correction,
        measuring_head_180,
        False,
        isLidar3reversed,
    )

    # Preprocess the reference lidar pointcloud
    preprocessed_pointcloud = preprocess_pointcloud(
        np.array(
            [
                ref_lidar_pointcloud["pt_x"],
                ref_lidar_pointcloud["pt_y"],
                ref_lidar_pointcloud["pt_z"],
            ]
        ).T
    )

    return [preprocessed_pointcloud, lidars["lms_1"], lidars["lms_3"], h5_info]


def cost_funct(transformation_parameters, *args):
    # Read the arguments
    start_time = time.time()
    input = args[0]

    transf = input["lidar_data"]
    reversed = input["reversed"]
    ref_lidar_pointcloud = input["ref_lidar_pointcloud"]
    ref_lidar_data = input["ref_lidar_data"]
    h5_info = input["h5_info"]
    imu_correction = input["imu_correction"]
    measuring_head_180 = input["measuring_head_180"]

    dx = transformation_parameters[0]
    dy = transformation_parameters[1]
    dz = transformation_parameters[2]
    yaw = transformation_parameters[3]
    pitch = transformation_parameters[4]
    roll = transformation_parameters[5]

    matrix = utils.compute_transformation_matrix(dx, dy, dz, yaw, pitch, roll)

    # Get the lidar pointcloud with the matrix computed using the new parameters
    lidar_pointcloud = LidarCalc.compute_lidar_positions(
        ref_lidar_data,
        matrix,
        transf,
        Geo.to_dict_array(h5_info.geo),
        imu_correction,
        measuring_head_180,
        True,
        reversed,
    )

    # Preprocess the lidar pointcloud
    preprocessed_pointcloud = preprocess_pointcloud(
        np.array(
            [
                lidar_pointcloud["pt_x"],
                lidar_pointcloud["pt_y"],
                lidar_pointcloud["pt_z"],
            ]
        ).T
    )

    # Cost is the RMSE of the distance between
    # each corresponding points of the reference lidar pointcloud and the other lidar pointcloud
    dists = ref_lidar_pointcloud.compute_point_cloud_distance(preprocessed_pointcloud)
    dists = np.asarray(dists)
    rmse = mean_squared_error(dists, np.zeros(len(dists)), squared=False)

    cost = rmse
    global iteration

    print(
        "cost: "
        + str(cost)
        + ", iteration: "
        + str(iteration)
        + ", time: "
        + str(time.time() - start_time)
    )

    print([dx, dy, dz, yaw, pitch, roll])

    iteration += 1

    return cost


parser = argparse.ArgumentParser()
parser.add_argument("--h5_path", required=True)
parser.add_argument("--output")
parser.add_argument("--lidar", required=True)
# Boolean arguments
parser.add_argument("--imu_correction", action="store_true")
parser.add_argument("--no_imu_correction", dest="imu_correction", action="store_false")
parser.add_argument("--reversed", action="store_true")
parser.add_argument("--no_reversed", dest="reversed", action="store_false")
parser.add_argument("--measuring_head_180", action="store_true")
parser.add_argument(
    "--no_measuring_head_180", dest="measuring_head_180", action="store_false"
)
parser.set_defaults(imu_correction=False)
parser.set_defaults(measuring_head_180=False)
parser.set_defaults(reversed=False)

args = parser.parse_args()

# Create output folder
if not os.path.exists(args.output):
    os.makedirs(args.output)

# Transformation matrix initial parameters
# If these parameters are not close to the real parameters
# the algorithm will probably not converge
initial_parameters = [
    0.284,  # dx
    -1,  # dy
    0,  # dz
    -math.pi,  # Yaw
    0,  # Pitch
    -math.pi / 4,  # Roll
]

# Extract the pointcloud from the reference lidar one time so we don't have to
# extract it at each iteration
ref_lidar_pointcloud, ref_lidar_data, lidar_data, h5_info = get_lidars_data(
    args.h5_path, args.reversed, args.imu_correction, args.measuring_head_180
)

arguments = {
    "lidar_data": lidar_data,
    "reversed": args.reversed,
    "ref_lidar_pointcloud": ref_lidar_pointcloud,
    "ref_lidar_data": ref_lidar_data,
    "h5_info": h5_info,
    "imu_correction": args.imu_correction,
    "measuring_head_180": args.measuring_head_180,
}

# Parameters optimization by minimizing a cost
# using Nelder-Mead max number of iteration is the number of parameters * 200
# In our case the max number of iteration will be 600
optimized_parameters = optim.minimize(
    fun=cost_funct, x0=initial_parameters, args=arguments, method="Nelder-Mead"
)

optimized_matrix = utils.compute_transformation_matrix(
    optimized_parameters.x[0],
    optimized_parameters.x[1],
    optimized_parameters.x[2],
    optimized_parameters.x[3],
    optimized_parameters.x[4],
    optimized_parameters.x[5],
)

np.savetxt(os.path.join(args.output, "transformation_matrix.txt"), optimized_matrix)

parameters = np.array(
    [
        optimized_parameters.x[0],
        optimized_parameters.x[1],
        optimized_parameters.x[2],
        optimized_parameters.x[3],
        optimized_parameters.x[4],
        optimized_parameters.x[5],
    ]
)

np.savetxt(os.path.join(args.output, "parameters.txt"), parameters)
