import numpy as np
import open3d as o3d
import CSF


def remove_ground(pointcloud):
    print("Removing ground")
    csf = CSF.CSF()

    # prameter settings
    csf.params.bSloopSmooth = False
    csf.params.cloth_resolution = 0.1
    csf.params.rigidness = 3

    csf.params.time_step = 0.4
    csf.params.class_threshold = 0.1
    # more details about parameter: http://ramm.bnu.edu.cn/projects/CSF/download/

    csf.setPointCloud(pointcloud)
    ground = (
        CSF.VecInt()
    )  # a list to indicate the index of ground points after calculation
    non_ground = (
        CSF.VecInt()
    )  # a list to indicate the index of non-ground points after calculation
    csf.do_filtering(ground, non_ground)  # do actual filtering.

    return pointcloud[non_ground]


def preprocess_pointcloud(pointcloud, threshold: int = 0.1):
    """Removes the ground and denoise the pointcloud"""

    """ Remove the lidar trajectory points """
    #Find the heighest point in the pointcloud keep all the point below this point
    max_z = np.max(pointcloud[:, 2])
    pointcloud = pointcloud[np.where(pointcloud[:, 2] < max_z - threshold)]

    """ Remove Ground """
    #Removing ground using CSF
    pointcloud = remove_ground(pointcloud)

    #Convert to Open3D pointcloud
    o3d_pointcloud = o3d.geometry.PointCloud()
    o3d_pointcloud.points = o3d.utility.Vector3dVector(pointcloud)

    print("Denoising")
    #Denoising using SOR
    _, ind = o3d_pointcloud.remove_statistical_outlier(nb_neighbors=20, std_ratio=0.5)
    o3d_pointcloud = o3d_pointcloud.select_by_index(ind)

    return o3d_pointcloud
